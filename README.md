# tarNSx

```
Copyright (C) 2023 tz@uni.kn
Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
```

Tool to unzip and split compressed NSP files (NSZ), or concat and zip NSP files.
Depends on [nsz](https://github.com/nicoboss/nsz.git). Optionally uses [splitNSP](https://github.com/AnalogMan151/splitNSP.git) if available, else just uses `split`.

## Installation

Since distros move avay from `pip`, this tool and dependencies are best installed in a virtual environment. You might need `python-venv` or similar.

```
# create and prepare venv
python -m venv pyvenv
source pyvenv/bin/activate
mkdir -p pyvenv/opt
cd pyenv/opt

# install nsz
pip install nsz

# optionally install splitNSP
git clone https://github.com/AnalogMan151/splitNSP.git
ln -s ../opt/splitNSP/splitNSP.py ../bin/
cd ..

# intall tarNSx
git clone https://gitlab.com/teezee/tarNSx.git
ln -s ../opt/tarNSx/tarNSx.sh ../bin/
```

## Usage

```
usage: tarNSx.sh  <path-to-ns(p|z)>
```

