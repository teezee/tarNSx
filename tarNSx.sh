#!/bin/bash
# cat and split and zip and unzip nsp files
#
# Copyright (C) 2023 tz@uni.kn
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
#
# Uses the following projects
# nszgit = "https://github.com/nicoboss/nsz.git"
# splitNSPgit = "https://github.com/AnalogMan151/splitNSP.git"

usage() {
    echo "usage $(basename "$0") <path-to-ns(p|z)>"
    echo ""
    echo "Cats, splits, zips & unzips NSP/NSZ, depending on file" 
    exit 0
}

[[ "$#" -ne 1 ]] && usage;
inpath="$1"
[[ ! -e "${inpath}" ]] && usage;

if ! command -v nsz > /dev/null; then
    printf "nsz not found, trying to install ..."
    # this might fail for distros removing pip
    pip install nsz
fi

bname="$(basename "${inpath}")"
dname="$(dirname "${inpath}")"
pext="${bname##*.}"
pname="${bname%.*}"
catname="${dname}/${pname}_cat.nsp"
splitname="${dname}/${pname}_split.nsp"
#splitsize=4294967295 # ==$((2**32-1)), max of fat32 
splitsize=4294901760 # what splitNSP and nxdump uses
outprefix=""

if [ -f "${inpath}" ]; then
    # found file, assume either zip, or unzip
    if [ "${pext}" = "nsz" ]; then
        printf "nsz detected, unzipping ...\n"
        op=unzip-nsz
    elif [ "${pext}" = "nsp" ]; then
        printf "nsp detected, zipping ...\n"
        op=zip-nsp 
    fi
elif [ -d "${inpath}" ]; then
    printf "split nsp detected, cat and zip ...\n"
    op=zip-nsp
fi

cat-nsp() {
    if [ -d "${inpath}" ]; then
        cat "${inpath}"/?? > "${catname}"
    else
        printf "%s not a directory, can\'t cat." "${inpath}"
    fi
}

zip-nsp() {
    local infile="${inpath}"
    if [ -d "${inpath}" ]; then 
        cat-nsp
        infile="${catname}"
    fi
    nsz -C -V "${infile}"
}

split-nsp() {
    local nspfile="${dname}/${pname}.nsp"
    local outdir="${splitname}"
    mkdir -v "${outdir}"
    #split --verbose --bytes=${splitsize} --numeric-suffixes=0 ${nspfile} ${outprefix}
    split --verbose --bytes=${splitsize} -d -a 2 "${nspfile}" "${outprefix}"
    mv -v [0-9][0-9] "${outdir}"/ 
}

splitNSP() {
    local nspfile="${dname}/${pname}.nsp"
    splitNSP.py "${nspfile}"
}

check-split() {
    local nspfile="${dname}/${pname}.nsp"
    local filesize
    filesize=$(stat -c%s "${nspfile}")
    if (( filesize > splitsize )); then
        printf "size too big for fat32, splitting ...\n"
        if command -v splitNSP.py >/dev/null; then
            splitNSP
        else
            split-nsp
        fi
    fi
}

unzip-nsz() {
    nsz -D "${inpath}"
    check-split
}

$op
